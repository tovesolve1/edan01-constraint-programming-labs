import org.jacop.constraints.Not;
import org.jacop.constraints.PrimitiveConstraint;
import org.jacop.constraints.XgtC;
import org.jacop.constraints.XltC;
import org.jacop.constraints.XlteqC;
import org.jacop.core.FailException;
import org.jacop.core.IntDomain;
import org.jacop.core.IntVar;
import org.jacop.core.Store;

//import SimpleDFS.ChoicePoint;

public class SplitSearch_not_working {

	boolean trace = true;

	/**
	 * Store used in search
	 */
	Store store;

	/**
	 * Defines varibales to be printed when solution is found
	 */
	IntVar[] variablesToReport;

	/**
	 * It represents current depth of store used in search.
	 */
	int depth = 0;

	/**
	 * It represents the cost value of currently best solution for FloatVar cost.
	 */
	public int costValue = IntDomain.MaxInt;

	/**
	 * It represents the cost variable.
	 */
	public IntVar costVariable = null;

	/**
	 * Number of visited nodes
	 */
	public long N = 0;

	/**
	 * Number of failed nodes excluding leave nodes
	 */
	public long failedNodes = 0;

	public SplitSearch_not_working(Store s) {
		store = s;
	}

	/**
	 * This function is called recursively to assign variables one by one.
	 */
	public boolean label(IntVar[] vars) {

		N++;

		if (trace) {
			for (int i = 0; i < vars.length; i++)
				System.out.print(vars[i] + " ");
			System.out.println();
		}

		ChoicePoint choice = null;
		boolean consistent;

		// Instead of imposing constraint just restrict bounds
		// -1 since costValue is the cost of last solution
//		if (costVariable != null) {
//			try {
//				if (costVariable.min() <= costValue - 1)
//					costVariable.domain.in(store.level, costVariable, costVariable.min(), costValue - 1);
//				else
//					return false;
//			} catch (FailException f) {
//				return false;
//			}
//		}

		consistent = store.consistency();

		if (!consistent) {
			// Failed leaf of the search tree
			// Inconsistent constraints
			return false;
		} else { // consistent

			// basfall. Lösning hittad, return false
			if (vars.length == 0) {
				// solution found
				// update cost
				System.out.println("cv" + costVariable);
				if (costVariable != null) {
					costValue = costVariable.min();
				}
				reportSolution();
				// store.impose(new XltC(costVariable, costValue));
				return costVariable == null; // true is satisfiability search and false if minimization

			}

			choice = new ChoicePoint(vars);
			IntVar[] vPrim = choice.getSearchVariables();
			levelUp();

			// Only one choice left, don't split
			if (choice.var.min() == choice.var.max()) {
				consistent = label(vPrim);

				if (consistent) {
					levelDown();
					return true;
				} else {
					// levelDown();
					return false;
				}
			} else {

				int mid = (choice.var.min() + choice.var.max()) / 2;
				//store.impose(choice.lessThanEqConstraint(mid));

				//levelUp();
				// Vet ej om detta är rätt. Ska vara V, är V'? getsearch som parameter?
				consistent = label(vars);

				if (consistent) {
					levelDown();
					return true;
				} else {
					failedNodes++;
					restoreLevel(); // tar bort det gamla
					// undo all decisions
					// store.impose(new Not(choice.lessThanEqConstraint(mid)));
					store.impose(choice.greaterThanConstraint(mid));
					consistent = label(vars);

					levelDown();
					return consistent;
				}
			}

		}

	}

	void levelDown() {
		store.removeLevel(depth);
		store.setLevel(--depth);
	}

	void levelUp() {
		store.setLevel(++depth);
	}

	void restoreLevel() {
		store.removeLevel(depth);
		store.setLevel(store.level);
	}

	public void reportSolution() {
		System.out.println("Nodes visited: " + N);

		if (costVariable != null)
			System.out.println("Cost is " + costVariable);

		for (int i = 0; i < variablesToReport.length; i++)
			System.out.print(variablesToReport[i] + " ");
		System.out.println("\n---------------");
	}

	public void setVariablesToReport(IntVar[] v) {
		variablesToReport = v;
	}

	public void setCostVariable(IntVar v) {
		costVariable = v;
	}

	public class ChoicePoint {

		IntVar var;
		IntVar[] searchVariables;
		int value;

		public ChoicePoint(IntVar[] v) {
			var = selectVariable(v);
			value = selectValue(var);
		}

		public IntVar[] getSearchVariables() {
			return searchVariables;
		}

		/**
		 * example variable selection; input order
		 */
		IntVar selectVariable(IntVar[] v) {
			if (v.length != 0) {

				searchVariables = new IntVar[v.length - 1];
				for (int i = 0; i < v.length - 1; i++) {
					searchVariables[i] = v[i + 1];
				}

				return v[0];

			} else {
				System.err.println("Zero length list of variables for labeling");
				return new IntVar(store);
			}
		}

		/**
		 * example value selection; indomain_min
		 */
		int selectValue(IntVar v) {
			return v.min();
		}

		/**
		 * example constraint assigning a selected value
		 */
		public PrimitiveConstraint lessThanEqConstraint(int c) {
			return new XlteqC(var, c);
			// return new XeqC(var, value);
		}

		public PrimitiveConstraint greaterThanConstraint(int c) {
			return new XgtC(var, c);
		}
	}
}
