import org.jacop.constraints.Not;
import org.jacop.constraints.PrimitiveConstraint;
import org.jacop.constraints.XeqC;
import org.jacop.constraints.XgteqC;
import org.jacop.constraints.XlteqC;
import org.jacop.core.FailException;
import org.jacop.core.IntDomain;
import org.jacop.core.IntVar;
import org.jacop.core.Store;


public class SplitSearch_t3 {

	boolean trace = false;

	/**
	 * Store used in search
	 */
	Store store;

	/**
	 * Defines varibales to be printed when solution is found
	 */
	IntVar[] variablesToReport;

	/**
	 * It represents current depth of store used in search.
	 */
	int depth = 0;

	/**
	 * It represents the cost value of currently best solution for FloatVar cost.
	 */
	public int costValue = IntDomain.MaxInt;

	/**
	 * It represents the cost variable.
	 */
	public IntVar costVariable = null;

	/**
	 * Number of visited nodes
	 */
	public long N = 0;

	/**
	 * Number of failed nodes excluding leave nodes
	 */
	public long failedNodes = 0;

	public SplitSearch_t3(Store s) {
		store = s;
	}

	/**
	 * This function is called recursively to assign variables one by one.
	 */
	public boolean label(IntVar[] vars) {
		N++;

		if (trace) {
			for (int i = 0; i < vars.length; i++)
				System.out.print(vars[i] + " ");
			System.out.println();
		}

		ChoicePoint choice = null;
		boolean consistent;

		// Instead of imposing constraint just restrict bounds
		// -1 since costValue is the cost of last solution
		if (costVariable != null) {
			try {
				if (costVariable.min() <= costValue - 1)
					costVariable.domain.in(store.level, costVariable, costVariable.min(), costValue - 1);
				else
					return false;
			} catch (FailException f) {
				return false;
			}
		}
		//pseudokod
		consistent = store.consistency();

		if (!consistent) {
			// Failed leaf of the search tree
			return false;
		} else { // consistent

			if (vars.length == 0) {
				// solution found; no more variables to label

				// update cost if minimization
				if (costVariable != null)
					costValue = costVariable.min();

				reportSolution();

				return costVariable == null; // true is satisfiability search and false if minimization
			}

			choice = new ChoicePoint(vars);

			levelUp();

			store.impose(choice.getConstraint());

			// choice point imposed.

			consistent = label(choice.getSearchVariables());

			if (consistent) {
				levelDown();
				return true;
			} else {
				failedNodes++;

				restoreLevel();

				store.impose(new Not(choice.getConstraint()));

				// negated choice point imposed.

				consistent = label(vars);

				levelDown();

				if (consistent) {
					return true;
				} else {
					return false;
				}
			}
		}
	}

	void levelDown() {
		store.removeLevel(depth);
		store.setLevel(--depth);
	}

	void levelUp() {
		store.setLevel(++depth);
	}

	void restoreLevel() {
		store.removeLevel(depth);
		store.setLevel(store.level);
	}

	public void reportSolution() {
		System.out.println("Nodes visited: " + N);

		if (costVariable != null)
			System.out.println("Cost is " + costVariable);

		for (int i = 0; i < variablesToReport.length; i++)
			System.out.print(variablesToReport[i] + " ");
		System.out.println("\n---------------");
	}

	public void setVariablesToReport(IntVar[] v) {
		variablesToReport = v;
	}

	public void setCostVariable(IntVar v) {
		costVariable = v;
	}

	public class ChoicePoint {

		IntVar var;
		IntVar[] searchVariables;
		int value;

		public ChoicePoint(IntVar[] v) {
			var = selectLargestOptionVariable(v);
			value = selectValue(var);
		}

		public IntVar[] getSearchVariables() {
			return searchVariables;
		}

		/**
		 * 
		 */
		IntVar selectFailFirstVariable(IntVar[] v) {
			if (v.length != 0) {
				int minIndex = findFailFirst(v);
				
				if(v[minIndex].max() == v[minIndex].min()) {
					searchVariables = new IntVar[v.length - 1];
					
					for (int i = 0; i < minIndex; i++) {
							searchVariables[i] = v[i];	
					}
					for (int i = minIndex; i < v.length - 1; i++) {
						searchVariables[i] = v[i + 1];
					}
					
					
					return v[findFailFirst(searchVariables)];
					
				} else {
					
					searchVariables = new IntVar[v.length];
					searchVariables = v;
					return v[minIndex];
					
				}

			} else {
				System.err.println("Zero length list of variables for labeling");
				return new IntVar(store);
			}
		}
		
		IntVar selectSmallestOptionVariable(IntVar[] v) {
			if (v.length != 0) {
				int minIndex = findSmallest(v);
				
				if(v[minIndex].max() == v[minIndex].min()) {
					searchVariables = new IntVar[v.length - 1];
					
					for (int i = 0; i < minIndex; i++) {
							searchVariables[i] = v[i];	
					}
					for (int i = minIndex; i < v.length - 1; i++) {
						searchVariables[i] = v[i + 1];
					}
					
				} else {
					
					searchVariables = new IntVar[v.length];
					searchVariables = v;
					
				}
				return v[findSmallest(searchVariables)];

			} else {
				System.err.println("Zero length list of variables for labeling");
				return new IntVar(store);
			}
		}
		
		
		IntVar selectLargestOptionVariable(IntVar[] v) {
			if (v.length != 0) {
				int minIndex = findLargest(v);
				
				if(v[minIndex].max() == v[minIndex].min()) {
					searchVariables = new IntVar[v.length - 1];
					
					for (int i = 0; i < minIndex; i++) {
							searchVariables[i] = v[i];	
					}
					for (int i = minIndex; i < v.length - 1; i++) {
						searchVariables[i] = v[i + 1];
					}
					
				} else {
					
					searchVariables = new IntVar[v.length];
					searchVariables = v;
					
				}
				return v[findLargest(searchVariables)];

			} else {
				System.err.println("Zero length list of variables for labeling");
				return new IntVar(store);
			}
		}

		/**
		 * example value selection; indomain_min
		 */
		//select midpoint
		int selectValue(IntVar v) {
			int mid = (v.min() + v.max()) / 2;
			return mid;
		}

		/**
		 * example constraint assigning a selected value
		 */
		public PrimitiveConstraint getConstraint() {
			return new XlteqC(var, value);
		}
		
		
		private int findFailFirst(IntVar[] v) {
			int minLength = Integer.MAX_VALUE;
			int minIndex = 0;
			for (int i = 0; i < v.length; i++) {
				if (v[i].getSize() < minLength) {
					minLength = v[i].getSize();
					minIndex = i;
				}
			}
			return minIndex;
		}
		
		private int findSmallest(IntVar[] v) {
			int minValue = Integer.MAX_VALUE;
			int index = 0;
			for (int i = 0; i < v.length; i++) {
				if(v[i].min() < minValue){
					minValue = v[i].min();
					index = i;
				}
			}
			return index;
		}
		
		private int findLargest(IntVar[] v) {
			int maxValue = Integer.MIN_VALUE;
			int index = 0;
			for (int i = 0; i < v.length; i++) {
				if(v[i].min() > maxValue){
					maxValue = v[i].min();
					index = i;
				}
			}
			return index;
		}
	}
}
